.. index::
   pair: Floriane Chinsky; Dernière chance pour s’inscrire ! Du Love** ❤️ et du Lol 😹contre la haine en ligne


.. _chinsky_2024_02_04:

===================================================================================================
2024-02-04 **Dernière chance pour s’inscrire ! Du Love** ❤️ et du Lol 😹contre la haine en ligne
===================================================================================================

- https://rabbinchinsky.fr/2024/02/04/derniere-chance-pour-sinscrire-du-love-%e2%9d%a4%ef%b8%8f-et-du-lol-%f0%9f%98%b9contre-la-haine-en-ligne/

Tu ne 👍 pas les commentaires haineux sur les réseaux? Tu as besoin de ❤️
et de 😹 pour garder ta force d’action? Rejoins-nous le 5 février 2024, pour agir
concrètement contre toutes les haines en ligne.

Rire, amour et activisme garanti. Venez avec vos téléphones pour la partie action concrète!

Inscription sur ce lien https://framaforms.org/du-love-et-du-lol-contre-la-haine-en-ligne-1705503802


Aux manettes
================

- Shani Benoualid, co-fondatrice de l’association #jesuislà (qui lutte contre la haine en ligne).
- Floriane Chinsky, rabbine à Judaïsme En Mouvement et formatrice en Écoute Mutuelle
- Laura Domenge, humoriste.

L’objectif
=====================

Partager avec une soixantaine de participant.es des outils et des conseils
pratiques pour prendre part activement à la lutte contre la haine et la
désinformation en ligne.

Un moment d’échange et d’apprentissage chaleureux, placé sous le signe de l’humour.


L’objectif
=====================

Partager avec une soixantaine de participant.es des outils et des conseils
pratiques pour prendre part activement à la lutte contre la haine et la
désinformation en ligne.

Un moment d’échange et d’apprentissage chaleureux, placé sous le signe de l’humour.


Au programme
========================

Centrage pour prendre soin de soi et des autres, panel avec les asso, action
concrète.

Pour finir, un Houmous préparé par le traiteur d’insertion « La table du Recho ».
Et l’humour? Il nous accompagnera en continu!

Nous avons également sollicité différentes assos engagées contre la haine,
on vous en dit plus très bientôt.

