.. index::
   pair: Golem Théâtre ; ROUAGES Un homme et une femme dans la tourmente des procès staliniens en Tchécoslovaquie (2024-01-20)
   ! Golem Théâtre

.. _golem_theatre_2024_01_20:

==============================================================================================================================================
|GolemTheatre| 2024-01-20  Golem théâtre **ROUAGES Un homme et une femme dans la tourmente des procès staliniens en Tchécoslovaquie**
==============================================================================================================================================

- https://hoteleuropa.fr/
- https://hoteleuropa.fr/rouages/
- https://hoteleuropa.fr/la-compagnie-golem-theatre/


.. figure:: images/golem_theatre.png


Dossier au format PDF
=========================

- https://hoteleuropa.fr/wp-content/uploads/2023/05/Rouages-dossier-DEF.pdf

|GolemTheatre| Compagnie Golem Théâtre
============================================

Créée à Prague par Michal Laznovsky et Frederika Smetana, la compagnie
Golem Théâtre a été rapidement accueillie par des scènes françaises et
est aujourd’hui implantée dans le Trièves (Isère).

Elle s’intéresse à des thématiques en lien avec l’Histoire et la Mémoire
et mène, depuis plusieurs années, un projet, L’Europe sans bagage, en lien
avec des historiens et des lieux de Mémoire.

Golem théâtre a été accueilli par La Filature de Mulhouse, la Halle aux
Grains de Blois, le théâtre Toursky à Marseille, le théâtre des Célestins à Lyon.

Deux créations, « Héritage de feu » d’après le récit de Friedelind Wagner
et « La guerre des Salamandres » d’après Karel Capek, ont été réalisées en
coproduction avec l’Opéra de Dijon.

Le spectacle « Casablanca 41 », de Michal Laznovsky a été nominé par le
Club de la Presse du Festival d’Avignon parmi les dix meilleures créations
du OFF 2016.

Texte
=======

- https://hoteleuropa.fr/rouages/
- https://hoteleuropa.fr/wp-content/uploads/2023/05/Rouages-dossier-DEF.pdf

**Michal Laznovsky** d'après les témoignages de Heda Margolius-Kovaly
Heda Margolius-Kovaly, Evzen Löbl et Arthur London


Heda Margolius-Kovaly
---------------------------

Heda Margolius Kovály publie au Canada en 1973 un récit autobiographique
« Na vlastni kuzi » (De première main).
Ce n’est qu’après 1989 que le livre paraît à Prague. Il est au cœur de
notre spectacle.

Le texte theatral s’appuie sur son recit des evenements
--------------------------------------------------------

Le texte theatral s’appuie sur son recit des evenements de l’année 1952,
avec l’arrestation, le procès, la condamnation et l’exécution
de son mari, Rufolf Margolius, alors vice-ministre du commerce extérieur,
aux côtés de treize autres dignitaires du Parti communiste et du gouvernement,
accusés de trahison, de complot contre l’Etat, d’intelligence avec les
puissances impérialistes et Israël.

La majorité d’entre eux sont juifs et leur origine sera largement utilisée
dans les actes d’accusation

Au terme d’un simulacre de proces pilote par staline,
----------------------------------------------------------

onze d’entre eux sont condamnés à mort, trois autres à la prison à perpétuité.
Parmi eux, Eugen Löbl, dont le témoignage nous fait pénétrer dans la vie
des prisonniers et Artur London, l’auteur du livre « L’aveu », dont
l’adaptation filmique de Costa-Gavras fera connaître au grand public
l’histoire des procès de Prague.


Collaboration dramaturgique et traductions
============================================

Frederica Smetana
------------------------

Après une formation au CNR de Nice, elle entre à l’Académie Supérieure
de Théâtre de Prague.
À Paris, elle suit les cours de Niels Arestrup, Philippe Minyana, Francine Bergé
à l’École du Passage.
Elle a travaillé avec Petr Forman et Ivo Krobot au Théâtre National
de Prague. Elle a interprété le rôle de Jeanne d’Arc dans l’oratorio de
Honegger-Claudel aux côtés de Michel Favory, de la Comédie française,
sous la direction de Serge Baudo.

Après la Révolution de velours, elle devient responsable de la programmation Théâtre
et Danse aux côtés d’Olivier Poivre d’Arvor, à l’Institut français de Prague.
Elle a assisté Daniel Mesguich pour la création de l’opéra de Laurent Petitgirard
« Elephant-man » à l’Opéra d’État de Prague, puis à l’Opéra de Nice.
Elle a traduit plusieurs textes de Michal Laznovsky ou d’auteurs tchèques
destinés aux créations de la compagnie.

Conseillère scientifique
============================

Françoise Meyer (Université Paul Valéry de Montpellier et chercheuse
associée au CERCEC (EHESS)

Actrice, acteurs
====================

Frederika Smetana
--------------------

.. figure:: images/frederika_smetana.png
   :width: 400

   Frederika Smetana


- Bruno La Brasca
- Philippe Vincenot

.. figure:: images/actrices_acteurs.png

Dates du spectacle
========================

- https://hoteleuropa.fr/category/agenda/

Du 22 au 24 mars 2024 Crearc-Le Petit Théâtre-Grenoble
---------------------------------------------------------

- https://crearc.fr/le-petit-theatre/

::

    8 rue Pierre Duclot
    38000 GRENOBLE
    FRANCE
    Tél : 04 76 01 01 41

.. raw:: html

   <iframe width="800" height="400" src="https://www.openstreetmap.org/export/embed.html?bbox=5.72862446308136%2C45.190993666403195%2C5.732164978981019%2C45.192530439534224&amp;layer=mapnik" style="border: 1px solid black">
   </iframe><br/><small><a href="https://www.openstreetmap.org/#map=19/45.19176/5.73039">Afficher une carte plus grande</a></small>

Le 20 janvier 2024 Médiathèque Kateb Yacine-Grenoble
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Le 24 mars 2023 Musée du Trièves-Mens
++++++++++++++++++++++++++++++++++++++++++++


Le 25 mars 2023 Association Franco-Tchèque-Lyon
+++++++++++++++++++++++++++++++++++++++++++++++++++++


Le 16 novembre 2023 Centre Tchèque-Paris
+++++++++++++++++++++++++++++++++++++++++++++



