.. index::
   pair: Ivan Segré ; Les deux pôles de l’identité juive (2024-01-17)

.. _segre_2024_01_17:

=======================================================================================================
2024-01-17 **Les deux pôles de l’identité juive** par Ivan Segré
=======================================================================================================

- https://k-larevue.com/les-deux-poles-de-lidentite-juive/

Un juif qui transgresserait le shabbat sans avoir conscience de son existence
devrait-il l’expier ? Partant de ce problème d’une judéité inconsciente
d’elle-même, Ivan Segré interroge la bipolarité de l’être juif,
entre la facticité de l’inscription généalogique et la radicalité de
l’affirmation subjective. Par-là il éclaire l’articulation juive entre
l’émancipation individuelle et collective : ce n’est pas parce qu’il se
sait juif que Moïse décide de quitter la maison de Pharaon mais, en posant
cet acte, il l’était pourtant déjà…


Pablo Picasso, ‘Autoportrait face à la mort’, 1972.



La question de l’identité juive peut être abordée de bien des manières,
depuis une approche juridique (halakhique) de « qui est juif » jusqu’à
la multiplicité des formes historiques, culturelles, politiques et sociales
de l’existence juive.

Il est également possible d’envisager la question par un biais rigoureusement
conceptuel. L’enjeu est alors d’interroger l’identité juive à partir
du corpus fondateur, Bible et Talmud. Et à embrasser la Torah d’un regard,
la singularité de l’identité juive réside dans son articulation à deux
événements fondateurs : le départ d’Abraham « vers la terre que je te
montrerai » (Genèse) et la sortie d’Égypte (Exode).

Cette double fondation recèle un enseignement : la question de l’identité,
dans le judaïsme, est nouée à celle de l’émancipation individuelle
et collective. C’est sans doute la première des raisons pour laquelle
la singularité juive a une valeur universelle, au sens où les prophètes
évoquent une « lumière pour les nations ».

Ceci posé, nous voudrions interroger un énoncé du Talmud qui définit
l’identité juive suivant deux pôles, l’un purement généalogique,
l’autre purement subjectif.

Au traité talmudique Shabbat, folio 68, une discussion oppose deux des
principaux maîtres du judaïsme d’Eretz-Israël (dit « palestinien »
suivant le lexique universitaire) à deux des principaux maîtres du judaïsme
babylonien. Un tel dispositif, peu commun, signale l’importance de la question
posée. Celle-ci concerne une notion en effet fondamentale, soit le fait de
transgresser la loi sans en avoir conscience (be-shogeg).

Le jour du shabbat, il est interdit, par exemple, de jardiner ; si Lévi jardine
néanmoins, parce qu’il a oublié qu’aujourd’hui est le jour de shabbat,
alors il a agi be–shoggeg, sans avoir conscience de transgresser l’interdit
du jour de shabbat. Il doit dans ce cas apporter un sacrifice au Temple pour
expier, réparer ou racheter son inconscience, soit le fait qu’il a oublié
que ce jour-là était le jour de shabbat.

La question débattue par ces maîtres du Talmud est la suivante : qu’en
est-il dans le cas d’un juif qui aurait ignoré le principe même du shabbat,
c’est-à-dire qui n’aurait jamais su qu’il y a un jour nommé « shabbat
» lors duquel on ne jardine pas ? Est-ce qu’il doit apporter un sacrifice
expiatoire pour réparer son inconscience ? Ou bien n’y-a-t-il inconscience,
et donc exigence d’expiation, que s’il a su, en un point au moins de son
existence consciente, qu’il existait un tel jour nommé « shabbat » et
qu’il a ensuite oublié l’existence de ce jour-là ?

Ou pour le traduire en termes heideggériens, l’oubli de la question de
l’être suppose-t-il qu’on ait été au moins une fois habité par cette
question, puis qu’on l’ait oubliée, ou bien l’oubli de cette question
vaut-il même sans que la question n’ait jamais consciemment été posée ?

Selon Rav et Shmuel, enseignants en Babylonie, un juif qui n’aurait jamais
su qu’il existe un jour du shabbat doit néanmoins, après qu’il en a pris
connaissance, apporter un sacrifice expiatoire en réparation de l’ensemble
des jours de shabbat dont il aura été inconscient. En revanche, selon Rabbi
Yohanan et Rech Laquich, enseignants en terre d’Israël, en un cas comme
celui-là l’inconscience du shabbat n’est pas significative puisque jamais
le juif en question n’a eu connaissance du shabbat, si bien qu’il n’y
a pas eu d’oubli.

Dans le Talmud, une question ne se pose que s’il existe un cas concret qui
en agence les termes. Comment situer concrètement le cas en question, celui
d’un « juif » qui n’aurait jamais rien su d’un jour nommé « shabbat
» ? La réponse du Talmud est la suivante : « un nourrisson kidnappé parmi
les idolâtres et un converti qui s’est converti parmi les idolâtres ».

En définissant le cas d’école dont il est question, le Talmud définit d’un
même pas l’identité juive au sens le plus strict, ou minimal, c’est-à-dire
une fois cette identité ramenée à son ossature existentielle irréfragable,
à sa « côte » fondatrice, à partir de laquelle bâtir. Et cette identité
minimale est donc duale : soit « un nourrisson kidnappé parmi les idolâtres
», soit « un converti qui s’est converti parmi les idolâtres ».

Le cas du « nourrisson kidnappé parmi les idolâtres » est immédiatement
clair et distinct : un homme juif et une femme juive – disons Amram et
Jokebed – ont eu un enfant qui, peu après sa naissance, a été kidnappé
puis élevé parmi des idolâtres. Il reste l’enfant de Jokebed et Amram,
un juif à part entière, bien qu’il ignore être juif (puisqu’il ne sait
rien de ses géniteurs) et qu’il ignore même, possiblement, l’existence
des juifs (puisqu’il a grandi dans un monde idolâtre). Réintégrant un beau
jour la communauté d’Israël et apprenant l’existence du « shabbat »,
doit-il apporter un sacrifice expiatoire ?

Ce premier pôle de l’identité juive éclaire la portée rigoureusement et
comme irréversiblement généalogique du nom « Israël » : est juif l’enfant
engendré par des parents juifs (ou a minima par une mère juive). C’est
l’une des deux dimensions de l’existence juive. Benny Lévy, dans Être
juif (Verdier, 2003), a proposé de nommer « facticité juive » ce premier
pôle du nom « Israël ».

Mais il y a donc un autre pôle, celui du « converti qui s’est converti parmi
les idolâtres ». Et ce deuxième terme du couple théorique en question a
posé un redoutable problème aux commentateurs médiévaux du Talmud : quel
est le cas de ce « converti qui s’est converti parmi les idolâtres »,
tant et si bien qu’il est devenu « juif » sans pourtant rien apprendre
d’un jour nommé « shabbat » ?

Le premier réflexe, devant une difficulté comme celle-ci, est de consulter le
commentaire de Rashi (XIe siècle). Mais à ce sujet, Rashi, vraisemblablement
de manière délibérée, ne dit rien, laissant l’étudiant réfléchir par
lui-même. Voyons donc ce qu’en disent les Tossaphistes.

Selon les Tossaphistes (XIIe siècle), dont la méthodologie, les modes de
conceptualisation et les conclusions ont pour l’essentiel déterminé la
pensée rabbinique postérieure en matière d’études talmudiques et de
jurisprudence, toute conversion au judaïsme suppose la présence de trois
juifs, ou tribunal rabbinique, outre la circoncision (dans le cas d’un
homme) et l’immersion dans un bain rituel. C’est en effet un enseignement
explicite du traité Yevamot, folios 46 et 47. Un enseignement de Rabbi Yehuda
précise même, folio 47a du dit traité, qu’une conversion devant être
réalisée devant un tribunal rabbinique, une conversion « de soi à soi »
ne vaut rien. Autrement dit, une conversion en son âme et conscience n’est
effective que si elle est réalisée en présence de (trois) juifs.

Le problème qui se pose aux Tossaphistes au traité Shabbat folio 68,
c’est donc que ce « converti qui s’est converti parmi les idolâtres »
s’apparente, de prime abord, au cas d’une conversion « de soi à soi »,
puisqu’à prendre l’énoncé à la lettre, sa conversion n’est pas
réalisée en présence de juifs mais dans un environnement exclusivement
composé d’idolâtres. Il s’agirait donc d’une personne qui, née de
parents idolâtres, ayant grandi dans un monde idolâtre, s’en affranchit par
elle-même et subjective, de soi à soi, le nom Israël, au point de devenir «
juif ». Or, d’après le traité Yevamot 47, une telle conversion « de soi
à soi », sans la présence de trois juifs composant un tribunal rabbinique,
n’est pas recevable. Aussi, comment expliquer que dans le cas de ce «
converti qui s’est converti parmi les idolâtres », sa conversion soit
effective, tant et si bien que les principaux maîtres du judaïsme palestinien
et babylonien se demandent, au traité Shabbat 68, si un tel « converti » doit
ou non apporter un sacrifice expiatoire (une fois qu’il aura eu connaissance,
un temps après sa conversion, de l’existence d’un jour nommé « shabbat
») ? Dès lors qu’il faut être un « juif » (ou un « israël » dans le
lexique du Talmud) pour être concerné par les interdictions relatives au
jour du shabbat, la question ne peut se poser que si ces maîtres du Talmud
reconnaissent en lui un « juif » au même titre qu’ils reconnaissent «
juif » l’enfant d’Amram et Jokebed kidnappé par des idolâtres.

Pour résoudre l’apparente contradiction entre l’enseignement du traité
Shabbbat 68 et celui de Yevamot 47, les Tossaphistes expliquent que dans le cas
de ce singulier converti « parmi les idolâtres », il y a eu une conversion
en bonne et due forme, c’est-à-dire devant un tribunal rabbinique. Mais
dans ce cas comment s’explique-t-on que le « converti » ignore le jour du
shabbat ? Les Tossaphistes répondent, contraints, que le tribunal rabbinique
aura omis de lui enseigner le jour du shabbat.

Peut-on se satisfaire de cette explication ? À notre sens, le commentaire
des Tossaphistes souligne la difficulté davantage qu’il ne la résout. Car
s’il s’agit d’une conversion devant un tribunal rabbinique, pourquoi
évoquer un « converti qui s’est converti parmi les idolâtres » ? En
outre, quel serait l’intérêt, pour le Talmud, de recourir à ce second cas,
celui d’un tribunal rabbinique qui omet d’enseigner le jour du shabbat
à un converti, puis l’abandonne parmi les idolâtres, seul, ignorant de
tout ?  Dès lors que la question qui importe au Talmud est celle d’un
juif inconscient de sa judéité (et partant, inconscient de sa relation au
shabbat), le premier cas suffit. S’il importe donc d’associer au cas du
nourrisson kidnappé un autre cas, ce n’est pas dans un souci de recourir
à toutes les configurations possibles et imaginables, mais dans l’idée
d’appréhender l’ossature irréfragable de l’identité juive. Or, quel
sens y-aurait-il à envisager trois juifs de passage dans un monde idolâtre,
convertissant un individu sans rien lui apprendre, puis repartant aussitôt
? Ce second cas, outre son excentricité, son inconsistance empirique,
n’a en soi aucune nécessité théorique, pas plus, donc, qu’il n’a
de nécessité existentielle. Néanmoins, selon les Tossaphistes, il faut se
résoudre à l’expliquer ainsi, sans quoi cet enseignement du traité Shabbat
68 contredirait celui du traité Yevamot 47. Or les enseignements du Talmud,
comme la géométrie euclidienne, font système.

Maïmonide (XIIe siècle), confronté au même problème, va tenter d’adoucir
la pente. Dans sa codification du Talmud (Halakhot Shegaggot, Chap. 7,
Halakha 2), il écrit que les deux cas en question sont ceux « d’un
enfant kidnappé parmi les idolâtres et d’un enfant converti parmi les
idolâtres ». Commentant le texte de Maïmonide, Rabbi Yossef Caro ne manque
pas de relever l’anomalie : le Talmud distingue entre le « nourrisson »
kidnappé parmi les idolâtres d’une part, le « converti » qui s’est
converti parmi les idolâtres d’autre part. Le second cas n’est donc pas,
a priori, celui d’un « enfant » mais aussi bien, ou exclusivement celui
d’un adulte. Rabbi Yossef Caro en conclut que selon Maïmonide, s’il
s’était agi d’un adulte, un tribunal rabbinique digne de ce nom lui aurait
nécessairement enseigné l’existence du shabbat. Et ce serait donc la raison
pour laquelle Maïmonide corrige le texte sur ce point : s’agissant d’un
enfant, on peut comprendre qu’on ne lui ait rien enseigné, ou que l’enfant
n’étant pas en âge de raison, l’enseignement est comme nul et non avenu,
si bien que cela équivaut au cas du nourrisson kidnappé. Mais s’agissant
d’un adulte, cela n’aurait aucun sens.

Reste que les versions du texte talmudique sont unanimes : il s’agit d’un «
converti » et non d’un « enfant converti ». Et pour ce qui est du premier
cas, il s’agit d’un « nourrisson » (tinok), et non d’un « enfant »
(katan). Le Talmud distingue donc très clairement les deux cas : l’un
est un « nourrisson kidnappé », n’ayant donc aucun souvenir conscient
de ses géniteurs ; l’autre est « un converti qui s’est converti », ce
qui dénote l’initiative personnelle d’un adulte. En outre, la pente, si
elle est adoucie là, demeure ici plus abrupte encore : comment s’expliquer
qu’une fois converti par un tribunal rabbinique, cet enfant soit abandonné
parmi les idolâtres ? Enfin, pourquoi le Talmud irait-il chercher un second
cas qui ne répond à aucune nécessité théorique (puisqu’il redouble le
premier), ni existentielle (puisqu’il est insensé) ?

À l’inverse, le cas du « nourrisson kidnappé parmi les idolâtres »
est d’une rigoureuse consistance théorique et existentielle : il expose la
dimension purement généalogique du nom « juif », l’individu en question
étant assigné à ce nom de manière passive, sans aucune initiative consciente
de sa part ; il y est voué de par sa naissance, d’où la « facticité juive
» dont parle Benny Lévy. Pourquoi donc ajouter le cas d’un converti dont on
ne sait guère comment déterminer les circonstances de la conversion, puis de
l’abandon par un tribunal rabbinique apparemment farfelu, sinon irresponsable ?

Nahmanide (XIIIe) propose une autre explication, en plus de celle avancée par
les Tossaphistes : il s’agirait bien d’une personne qui s’est converti
« de soi à soi » dans un monde idolâtre, mais « à qui on n’aurait
pas fait savoir qu’une conversion de soi à soi n’est pas une conversion
». Autrement dit, à suivre Nahmanide, il serait donc possible de s’en tenir
à la lettre du Talmud, à condition toutefois de préciser que la conversion
singulière de cet individu singulier aurait été dans ce cas validée alors
qu’elle n’aurait pas dû l’être.

Instruit de l’analyse des principaux commentateurs médiévaux, revenons
maintenant à l’énoncé du Talmud. L’individu en question « s’est
converti parmi les idolâtres », ce qui laisse entendre qu’il a agi de
sa propre initiative dans un monde idolâtre. Tandis que le premier pôle de
l’identité juive est celui de la généalogie, c’est-à-dire le cas du
nourrisson, irréversiblement juif bien que kidnappé et élevé parmi les
idolâtres, le second pôle serait donc, à suivre la lettre de l’énoncé
talmudique, celui de l’initiative personnelle, singulière, ne pouvant
prendre appui sur rien d’autre que sur soi, puisque l’individu en question
est né et vit « parmi les idolâtres ».

    Sur un pôle il y a l’irréfragable vocation intime de l’individu
    qui est né de parents juifs, sur un autre pôle il y a l’individu qui,
    singulièrement, extraordinairement, dans un monde idolâtre, a réactualisé
    la fondation abrahamique. Telle est donc, selon cet enseignement du Talmud,
    la dualité essentielle du nom « Israël » : à un pôle Abraham, à un
    autre le nourrisson capturé par des idolâtres, à savoir Moïse.

Le premier terme du couple théorique en question est donc l’individu qui,
né de parents juifs mais kidnappé dès sa naissance par des idolâtres,
apprend un jour qu’il est juif, qu’il est un enfant d’Israël. Être
juif, en ce sens, c’est être l’enfant de Jokebad et Amram, ce qu’il
a donc toujours été en son être même, en son réel le plus intime, mais
sans le savoir consciemment. Doit-il ou non, dans ce cas, apporter un sacrifice
expiatoire pour avoir ignoré le jour de shabbat, ou faut-il en un point de sa
vie consciente avoir su être juif et avoir su qu’existe le jour du shabbat
pour être redevable d’un tel sacrifice expiatoire ?

Le second pôle de l’identité juive, l’alter ego de l’enfant né
de parents juifs, c’est donc « un converti qui s’est converti parmi
les idolâtres ». Et à suivre la lettre du Talmud, ainsi que sa logique,
le cas est bel et bien celui d’un idolâtre qui devient un « israël »
par lui-même, par le fait de son propre cheminement existentiel. En effet,
s’il « s’est converti parmi les idolâtres », c’est donc qu’il
a vécu dans un monde idolâtre où il n’y avait nulle altérité juive,
nul juif de qui apprendre à désirer être juif, ni même de qui apprendre
qu’une telle nomination – « juif » ou « Israël » – existe, ainsi
qu’existe le jour de shabbat. Et pourtant, il s’est « converti ». Cela ne
voudrait donc pas nécessairement dire ici qu’il s’est « converti » au
judaïsme, puisqu’il ignore l’existence historique d’Israël. Cela veut
dire, a minima, qu’il s’est rendu absolument étranger au monde idolâtre
dans lequel il évolue – et qu’en ce sens il est devenu « juif ».

Le point d’identité logico-existentielle entre « le nourrisson kidnappé
» et « le converti parmi les idolâtres », ce serait donc que l’un et
l’autre sont rigoureusement inconscients d’être « juifs ».

La différence est cependant que « le nourrisson kidnappé parmi les
idolâtres » est « juif » du fait qu’il est l’enfant de Jokebed et
Amram, tandis que « le converti qui s’est converti parmi les idolâtres »
est « juif » du fait de sa propre création subjective, c’est-à-dire du
fait d’un cheminement existentiel singulier par lequel il s’est rendu
absolument étranger au monde idolâtre. Et tels seraient donc les deux
pôles de l’identité juive qui, indissolublement liés l’un à l’autre,
ouvriraient à la pensée en intériorité du nom « Israël » : le nourrisson
kidnappé, généalogiquement « juif », et le converti parmi les idolâtres,
subjectivement « juif ».

Abordons à présent l’objection des Tossaphistes. Une conversion au judaïsme
suppose, pour être effective, un tribunal rabbinique, soit la présence de
trois juifs, outre la circoncision et l’immersion. Or un tel rituel, s’il
est en effet nécessaire, comme l’enseigne le traité Yevamot 47, rend
impossible, ou « illégal », le cas du converti qui, parmi les idolâtres,
se serait converti « de soi à soi » en s’arrachant subjectivement au monde
dans lequel il vit, puisque dans son cas il n’y a ni tribunal rabbinique,
ni rite d’aucune sorte, et pour cause : le converti, en la circonstance,
ignore jusqu’à l’existence du shabbat, de la circoncision, du bain rituel
et même, à envisager les choses rigoureusement, des « juifs » comme du
nom « Israël » !

Résoudre le problème posé n’exige cependant pas, croyons-nous,
d’abandonner la cohérence théorique et existentielle de l’énoncé
talmudique. C’est au contraire en la maintenant que les choses
s’éclairent. En effet, dans le cas du traité Yevamot, il s’agit d’un
converti qui s’est converti parmi Israël, c’est-à-dire en étant au contact
avec des juifs, ou en étant au moins informé de l’existence historique
d’Israël, et c’est pourquoi sa conversion, dans ce cas, n’est recevable
que s’il est converti par un tribunal rabbinique et qu’il a procédé au
rite de la conversion (la circoncision et le bain rituel). Mais s’il s’est
converti de soi à soi, sa conversion n’est pas recevable, comme l’enseigne
Rabbi Yehuda en Yevamot 47a, car il se serait alors converti au judaïsme en
se défiant de l’existence historique des juifs.

En revanche, dans le cas d’un converti parmi les idolâtres, la conversion
« de soi à soi » est effective. Il est alors nommé « Israël » du fait de
sa propre création subjective, bien qu’il n’y ait ni tribunal rabbinique,
ni circoncision, ni bain rituel, ni shabbat. En effet, dans ce cas, il n’a
pas eu connaissance de l’altérité historique d’Israël, de ses maîtres,
de ses enseignements, de ses formes d’existence rituelle, etc., puisqu’il
vit « parmi les idolâtres ». Par conséquent, ce qui vaut dans ce cas,
c’est exclusivement sa puissance subjective singulière, par laquelle il
s’est arraché, par lui-même, de l’idolâtrie, et s’est rendu étranger
à son monde.

L’enseignement du traité Yevamot 47 s’articule donc à présent avec
celui du traité Shabbat 68, bien loin qu’ils se contredisent. Dans le
cas du traité Yevamot 47, il s’agit d’un converti qui a rencontré,
d’une manière ou d’une autre, l’altérité historique d’Israël,
le « fait juif », raison pour laquelle sa conversion « de soi à soi » ne
vaut pas, se serait-il circoncis, puis immergé dans un bain rituel, etc., car
sa conversion « de soi à soi » est alors l’expression d’une défiance
à l’égard de l’existence historique d’Israël.

Par différence, dans le cas du traité Shabbat 68, le « converti qui s’est
converti parmi les idolâtres », comme la lettre du texte le signale, est
un individu qui s’est affranchi par lui-même, par sa propre puissance
subjective, de l’idolâtrie, et cela sans rien savoir de l’existence
historique d’Israël. Or il est néanmoins considéré comme un « israël
» par les maîtres du Talmud, au même titre que le nourrisson kidnappé, si
bien que s’il apprend un jour l’existence historique d’Israël et celle
du jour du shabbat, et qu’il rejoint « ses frères », se posera alors la
question de savoir s’il doit apporter un sacrifice. Le « converti parmi
les idolâtres », ce serait donc chaque homme ou femme qui, singulièrement,
réactualise la fondation abrahamique : « Va pour toi, émancipe-toi de ton
devenir d’animal humain, émancipe-toi des déterminismes idolâtres ».

Les deux cas en question s’articulent à présent dans leur nécessité
théorique et existentielle : sur un pôle il y a l’irréfragable vocation
intime de l’individu qui est né de parents juifs, sur un autre pôle il y a
l’individu qui, singulièrement, extraordinairement, dans un monde idolâtre,
a réactualisé la fondation abrahamique. Telle est donc, selon cet enseignement
du Talmud, la dualité essentielle du nom « Israël » : à un pôle Abraham,
à un autre le nourrisson capturé par des idolâtres, à savoir Moïse.

Mais comment formaliser, en termes jurisprudentiels, un tel enseignement
? C’est sur ce point qu’ont buté, semble-t-il, les commentateurs médiévaux
: il est impossible de reconnaître un individu comme « juif » sur le seul
critère de son affranchissement subjectif eu égard au monde idolâtre. Et
de fait, dans son cas, nulle hypothétique tâche de naissance sur la fesse
droite ne saurait avérer la chose ! En ce sens, le cas du « converti qui
s’est converti parmi les idolâtres » est un réel qui résiste à toute
formalisation juridique. Reste que l’objectivation juridique de l’existence
juive (la codification médiévale et ses prolongements modernes) ne saurait
avoir le dernier mot. Car le Talmud est une pensée-pratique, non un code de
lois. Et en termes de pensée-pratique, le traité Shabbat 68 nous délivre un
enseignement biblico-talmudique des plus intimes, et des plus injonctifs : le
nourrisson Moïse et l’adulte Abraham sont les deux pôles de l’identité
juive.

Abraham, c’est donc le pôle de la subjectivation radicale, de la capacité
subjective de fonder à partir de rien, tandis que le nourrisson arraché
de ses parents juifs Amram et Jokebed, à savoir « Moïse l’égyptien »,
c’est le pôle de l’injonction, depuis l’intime, de revenir à Israël :
nourrisson remis aux mains de la fille de Pharaon pour échapper au décret de
mort, Moïse fut nommé par elle ; puis il vécut dans la maison du maître de
l’Égypte, jusqu’à ce que, devenu un homme, il réponde à la convocation
depuis l’intime : « Moïse devint grand, il sortit vers ses frères, il
vit l’oppression ».

L’identité juive réunit donc deux pôles : l’un procède de l’inconscient
d’une naissance irréversiblement vouée au nom « juif », l’autre procède
de la pure subjectivation consciente, sans laquelle il n’y a pas d’aventure
humaine véritablement sensée. Et à l’inverse de ce que l’argument
chrétien, depuis Paul de Tarse, n’a cessé de brandir à l’encontre
d’Israël, l’histoire de Moïse illustre précisément la conjonction
essentielle de ces deux pôles, naissance et subjectivation, puisque c’est
le départ abrahamique que Moïse, devenu homme, prolonge en rejoignant «
ses frères » – ce qui exigeait d’abord, dans son cas, de subjectiver, de
soi à soi, la nécessité existentielle de sortir de « la maison de Pharaon ».

Ivan Segré
