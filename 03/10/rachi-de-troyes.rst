.. index::
   pair: Rachi; 2024-03-10

.. _rachi_2024_03_10:

=======================================================================================================
2024-03-10 **Histoires : Rachi de Troyes**
=======================================================================================================

- https://www.france.tv/france-2/a-l-origine/5744643-histoires-rachi-de-troyes.html
- https://fr.wikipedia.org/wiki/Rachi


Qui est **Rabbi Chlomo ben Itzhak HaTzarfati, appelé plus communément Rachi** ?

Il est un rabbin, commentateur, talmudiste, poète français, né en 1040 à Troyes.

Il est surtout l’auteur de commentaires qui, aujourd’hui encore, dans le monde
entier, sont indissociables du texte Biblique.

Passionné de médecine, de commerce, de la culture, de la vigne, il a été l’un
des premiers auteurs à écrire en langue française alors que ses contemporains
utilisaient le latin.

Nous allons découvrir dans ce documentaire exceptionnel l’étendue de ce personnage
fascinant et majeur du judaïsme français.
